# Usage
# ./deploy_engine p3 api
# ./deploy_engine p4 event-commons
#
. ./util --source-only

# ip  and key are used to log into server.
ip=$(get "ip-list" $1)
key=$(get "ip-list" "$1_key")
env=$(get "ip-list" "$1_env")

#validate if ip and key are present
if [ -z "$ip" ] || [ -z "$key" ] || [ -z "$env" ]
then
	echo "ip or Key is missing."
	echo "ip: $ip"
	echo "key: $key"
	echo "env: $env"
 	exit 1
fi

#validate the config file
validate_config_file $2

# load all necessary variables
# BIN_HOME        : where java binaries are located
# SOURCE_HOME     : where java source files are located to build jar.
BIN_HOME='~/App'
SOURCE_HOME='~/.source'

app_name=$(get $2 "name")
jar_name=$(get $2 "jar")
branch=$(get $2 "branch")
source_type=$(get $2 "type")

app_bin=$BIN_HOME/$app_name
archived=$app_bin/archived

app_source=$SOURCE_HOME/$app_name

git_source=$(get $2 "git")


ssh -T -i "$key" "$ip" << EOF
 	# typeset is used to load necessary function from local to server location.
 	$(typeset -f start)

 	# check if bitbucket private key file is present.
	# as a private key is mandatory
	# for git pull 
	if [ ! -f ~/.ssh/bitbucket ] 
	then
		echo "Cannot proceed, (~/.ssh/bitbucket) bitbucket private key not found."
		exit 1
	fi

	# create all needed directories when deploying to new server.
	mkdir -p $BIN_HOME
	mkdir -p $SOURCE_HOME
	
	cd $SOURCE_HOME

	if [ ! -d $app_name ] 
	then 
		echo "[INFO] cloning..." 
		ssh-agent bash -c 'ssh-add ~/.ssh/bitbucket; git clone -b $branch $git_source'|| { echo "[ERROR] Git Clone failed."; exit 1; }
		echo "[INFO] Done." 
	fi

	cd $app_name
	
	echo "[INFO] Fetching latest commits..."
		ssh-agent bash -c 'ssh-add ~/.ssh/bitbucket; git pull origin $branch' || { echo "[ERROR] Git pull failed."; exit 1; }
	echo "[INFO] Done."
	
	echo "[INFO] Maven building..."
		mvn install -DskipTests || { echo "[ERROR] Build Failed."; exit 1; }
	echo "[INFO] Done."

	if [ "$source_type" == "dependency" ]; then
		echo "[INFO] Dependency updated."
	elif [ "$source_type" == "app" ]; then
		# If the package is executable then it is put in Seperate directory.
		mkdir -p $app_bin
		mkdir -p $archived
		
		echo "[INFO] Archiving previous build..."
			mv $app_bin/*.jar $archived/ 2>/dev/null
		echo "[INFO] Done."

		cp target/$jar_name $app_bin/
		echo "[INFO] Jar moved to $app_bin/"

		cd $app_bin
		echo "[INFO] Deploying..."	
			start $env $jar_name
		echo "[INFO] Done."
	fi

	echo -e "[INFO] \e[32;1m✓\e[0m 🤘 $2 deployed"
	echo "---------------------------------------------"